#include <stdio.h>

// TO DO: modify code so output goes to file named "out.txt" instead
// of to stdout (screen)

int main(int argc, char* argv[])
{
	if (argc < 2) {  //user forgot to give us a filename at command line
		printf("Usage: ./a.out filename\n");
		return 1; //exit program
	}


	FILE* infile = fopen(argv[1], "r");
	FILE* outfile = fopen("out.txt", "w");
	char a[6];

	while (fscanf(infile, "%5s", a) != EOF) {
		fprintf(outfile, "%s\n", a);
	}

	fclose(infile);
	fclose(outfile);
	return 0;

}
