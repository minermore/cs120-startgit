#include <stdio.h>
#include <math.h>
#include <assert.h>

// stdio for input & output functions
// math for pow, exp
// assert for assert

int main(int argc, const char *argv[])
{
    // Check command line arguments
    if(argc != 3) {
        puts("Error: program takes exactly two arguments");
        puts("Usage: ./a.out <input_file> <output_file>");
        return 1;
    }

    // TO DO: Open input file, handle errors

    // TO DO: Open output file, handle errors

    int line = 0;

    // TO DO: you can keep this loop or replace it -- up to you
    while(1) {
        line++;
        float p = 0.0;
        float r = 0.0;

        // TO DO: parse p, r, handle errors

        // assert(...) statements serve as "sanity checks"

        // A principal < 0 makes no sense, so we "assert" it is >= 0.
        // A rate <= 0 makes no sense, so we "assert" that it is > 0.
        // If the condition we assert is false, we crash with an error
        // message indicating which assertion failed.

        // Note: if an assert is testing a condition that might
        // legitimately be true due to user error, a normal if
        // statement and error message is better.  assert is for when
        // there's no sane situation where the condition should be
        // violated.

        assert(p >= 0.0);
        assert(r > 0.0);

        // Hint: use the pow(...) and exp(...) functions, declared in
        // math.h, for calculations below

        // TO DO: calculate amount with annual compounding
        float ci_annual = 0;
        assert(ci_annual >= p);

        // TO DO: calculate amount with monthly compounding
        float ci_monthly = 0;
        assert(ci_monthly >= ci_annual);

        // TO DO: calculate amount with continuous compounding
        float ci_cont = 0;
        assert(ci_cont >= ci_monthly);

        // TO DO: print 5-column, space-separated line to output file
    }

    // TO DO: final error checking; closing files

    // TO DO: return non-0 if error prevented us from completing
    return 0;
}


